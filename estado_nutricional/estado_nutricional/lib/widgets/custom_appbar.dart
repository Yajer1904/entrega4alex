import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({Key key, @required this.onPressed})
      : assert(onPressed != null),
        super(key: key);
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Estado Nutricional'),
          backgroundColor: Colors.deepPurple,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.refresh),
              onPressed: onPressed,
            )
          ],
        ),
        body: Center(
          child: AnimatedContainer(
            duration: Duration(seconds: 1),
          ),
        ));
  }

  @override
  Size get preferredSize {
    return new Size.fromHeight(20.0);
  }
}
